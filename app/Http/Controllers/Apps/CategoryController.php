<?php

namespace App\Http\Controllers\Apps;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Inertia\Inertia;

class CategoryController extends Controller
{
    # construct for middleware
    public function __construct()
    {
        $this->middleware('permission:categories.index')->only('index');
        $this->middleware('permission:categories.create')->only('create', 'store');
        $this->middleware('permission:categories.edit')->only('edit', 'update');
        $this->middleware('permission:categories.delete')->only('destroy');
    }

    # show all categories data
    public function index()
    {
        # get categories
        $categories = Category::when(request()->q, function ($categories) {
            $categories->where('name', 'like', '%' . request()->q . '%');
        })->latest()->paginate(20);

        # return with inertia
        return Inertia::render('Apps/Categories/Index', [
            'categories' => $categories,
        ]);
    }

    # view create form category
    public function create()
    {
        # return with inertia
        return Inertia::render('Apps/Categories/Create');
    }

    # action create category
    public function store(Request $request)
    {
        # validation
        $request->validate([
            'name' => ['required', 'min:3', 'max:255', Rule::unique('categories', 'name')],
            'image' => ['required', 'image', 'mimes:png,jpg,jpeg', 'max:2048'],
            'description' => ['required'],
        ]);

        # upload image handle
        $image = $request->file('image');
        $image->storeAs('categories/img', $image->hashName());

        # create category
        Category::create([
            'image' => $image->hashName(),
            'name' => $request->name,
            'slug' => str($request->name)->slug(),
            'description' => $request->description,
        ]);

        # redirect
        return to_route('apps.categories.index');
    }

    # view edit category
    public function edit(Category $category)
    {
        # return with inertia
        return Inertia::render('Apps/Categories/Edit', [
            'category' => $category
        ]);
    }


    # action update category
    public function update(Request $request, Category $category)
    {
        # validation
        $request->validate([
            'name' => ['required', 'min:3', 'max:255', Rule::unique('categories', 'name')->ignore($category)],
            'image' => ['nullable', 'image', 'mimes:png,jpg,jpeg', 'max:2048'],
            'description' => ['required'],
        ]);

        # update category
        # if image not update
        if ($request->file('image') == '') {
            $category->update([
                'name' => $request->name,
                'slug' => str($request->name)->slug(),
                'description' => $request->description,
            ]);
        # if image update 
        } else {
            Storage::disk('public')->delete('categories/img/' . basename($category->image));
            $image = $request->file('image');
            $image->storeAs('categories/img', $image->hashName());

            $category->update([
                'image' => $image->hashName(),
                'name' => $request->name,
                'slug' => str($request->name)->slug(),
                'description' => $request->description,
            ]);
        }

        # redirect
        return to_route('apps.categories.index');
    }

    # action delete category
    public function destroy(Category $category)
    {
        # delete category image
        Storage::disk('public')->delete('categories/img/' . basename($category->image));

        # action delete category
        $category->delete();

        # redirect
        return to_route('apps.categories.index');
    }
}
