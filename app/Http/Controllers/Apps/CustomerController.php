<?php

namespace App\Http\Controllers\Apps;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;
use Inertia\Inertia;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        # get customers
        $customers = Customer::when(request()->q, function ($customers) {
            $customers->where('name', 'like', '%' . request()->q . '%');
        })->latest()->paginate(20);

        # return with inertia
        return Inertia::render('Apps/Customers/Index', [
            'customers' => $customers
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        # return with inertia
        return Inertia::render('Apps/Customers/Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        # validation
        $request->validate([
            'name' => ['required', 'min:3', 'max:255'],
            'phone_number' => ['required'],
            'address' => ['required'],
        ]);

        # create customers
        Customer::create([
            'name' => $request->name,
            'phone_number' => $request->phone_number,
            'address' => $request->address,
        ]);

        # redirect
        return to_route('apps.customers.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        # return with inertia
        return Inertia::render('Apps/Customers/Edit', [
            'customer' => $customer
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        # validation
        $request->validate([
            'name' => ['required', 'min:3', 'max:255'],
            'phone_number' => ['required'],
            'address' => ['required'],
        ]);

        # update customer
        $customer->update([
            'name' => $request->name,
            'phone_number' => $request->phone_number,
            'address' => $request->address,
        ]);

        # redirect
        return to_route('apps.customers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        # delete customer
        $customer->delete();

        # redirect
        return to_route('apps.customers.index');
    }
}
