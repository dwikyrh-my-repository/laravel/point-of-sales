<?php

namespace App\Http\Controllers\Apps;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Inertia\Inertia;

class ProductController extends Controller
{
    # __construct for middleware
    public function __construct()
    {
        $this->middleware('permission:products.index')->only('index');
        $this->middleware('permission:products.create')->only('create', 'store');
        $this->middleware('permission:products.edit')->only('edit', 'update');
        $this->middleware('permission:products.delete')->only('destroy');
    }

    # show all products data
    public function index()
    {
        # get products
        $products = Product::when(request()->q, function ($products) {
            $products->where('name', 'like', '%' . request()->q . '%');
        })->latest()->paginate(20);

        # return with inertia
        return Inertia::render('Apps/Products/Index', [
            'products' => $products
        ]);
    }

    # view create product
    public function create()
    {
        # get categories data
        $categories = Category::all();

        # return with inertia
        return Inertia::render('Apps/Products/Create', [
            'categories' => $categories
        ]);
    }

    # action create product
    public function store(Request $request)
    {
        # validation
        $request->validate([
            'image' => ['required', 'image', 'mimes:png,jpg,jpeg', 'max:2048'],
            'barcode' => ['required', Rule::unique('products', 'barcode'), 'max:12'],
            'name' => ['required', 'min:3', 'max:255'],
            'category_id' => ['required'],
            'description' => ['required'],
            'buy_price' => ['required'],
            'sell_price' => ['required'],
            'stock' => ['required'],
        ]);

        # upload image handle
        $image = $request->file('image');
        $image->storeAs('products/img', $image->hashName());

        # create product
        Product::create([
            'image' => $image->hashName(),
            'barcode' => $request->barcode,
            'name' => $request->name,
            'slug' => str($request->name . ' ' . str()->random(5))->slug(),
            'description' => $request->description,
            'category_id' => $request->category_id,
            'buy_price' => $request->buy_price,
            'sell_price' => $request->sell_price,
            'stock' => $request->stock
        ]);

        # redirect
        return to_route('apps.products.index');
    }

    # view edit product
    public function edit(Product $product)
    {
        # get categories data
        $categories = Category::all();

        # return with inertia
        return Inertia::render('Apps/Products/Edit', [
            'product' => $product,
            'categories' => $categories,
        ]);
    }

    # action update product
    public function update(Request $request, Product $product)
    {
        # validation
        $request->validate([
            'barcode' => ['required', Rule::unique('products', 'barcode')->ignore($product), 'max:12'],
            'name' => ['required', 'min:3', 'max:255'],
            'category_id' => ['required'],
            'description' => ['required'],
            'buy_price' => ['required'],
            'sell_price' => ['required'],
            'stock' => ['required'],
        ]);

        # update category
        # if image no update
        if ($request->file('image') == '') {
            $product->update([
                'barcode' => $request->barcode,
                'name' => $request->name,
                'slug' => str($request->name . ' ' . str()->random(5))->slug(),
                'description' => $request->description,
                'category_id' => $request->category_id,
                'buy_price' => $request->buy_price,
                'sell_price' => $request->sell_price,
                'stock' => $request->stock
            ]);
        # if image update
        } else {
            Storage::disk('public')->delete('products/img/' . basename($product->image));
            $image = $request->file('image');
            $image->storeAs('products/img', $image->hashName());
            $product->update([
                'image' => $image->hashName(),
                'barcode' => $request->barcode,
                'name' => $request->name,
                'slug' => str($request->name . ' ' . str()->random(5))->slug(),
                'description' => $request->description,
                'category_id' => $request->category_id,
                'buy_price' => $request->buy_price,
                'sell_price' => $request->sell_price,
                'stock' => $request->stock
            ]);
        }

        # redirect
        return to_route('apps.products.index');
    }

    # action delete product
    public function destroy(Product $product)
    {
        # delete category image
        Storage::disk('public')->delete('products/img/' . basename($product->image));

        # delete product
        $product->delete();

        # redirect
        return to_route('apps.products.index');
    }
}
