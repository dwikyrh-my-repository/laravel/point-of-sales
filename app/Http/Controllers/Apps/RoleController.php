<?php

namespace App\Http\Controllers\Apps;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role as ModelsRole;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        # get roles
        $roles = ModelsRole::when(request()->q, function($roles) {
            $roles = $roles->where('name', 'like', '%'. request()->q . '%');
        })->with('permissions')->latest()->paginate(20);

        # return with inertia
        return Inertia::render('Apps/Roles/Index', [
            'roles' => $roles,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        # get all permissions
        $permissions = Permission::all();

        # render with inertia
        return Inertia::render('Apps/Roles/Create', [
            'permissions' => $permissions
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        # validation
        $request->validate([
            'name' => ['required', 'max:255'],
            'permissions' => ['required'],
        ]);

        # create role
        $role = ModelsRole::create(['name' => $request->name]);

        # assign permission to role
        $role->givePermissionTo($request->permissions);

        # redirect
        return to_route('apps.roles.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelsRole $role)
    {
        // $role = Role::with('permissions')->get();

        # get all permissions
        $permissions = Permission::all();

        # return with inertia
        return Inertia::render('Apps/Roles/Edit', [
            'role' => $role,
            'permissions' => $permissions,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelsRole $role)
    {
        # validation
        $request->validate([
            'name' => ['required'],
            'permissions' => ['required']
        ]);

        # update role
        $role->update(['name' => $request->name]);

        # sync permissions
        $role->syncPermissions($request->permissions);

        # redirect
        return to_route('apps.roles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelsRole $role)
    {
        # delete role
        $role->delete();

        # redirect
        return to_route('apps.roles.index');
    }
}
