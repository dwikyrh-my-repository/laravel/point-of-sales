<div class="title" style="padding-bottom: 13px">
  <div style="text-align: center;text-transform: uppercase;font-size: 15px">
      GreenK Cashier
  </div>
  <div style="text-align: center">
      Alamat: Jl. ABC No.97, Kecamatan Pamulang, Kota Bandung, Jawa Barat
  </div>
  <div style="text-align: center">
      Telephone: 08123456789
  </div>
</div>
<table style="width: 100%">
  <thead>
      <tr style="background-color: #D7DBDD;">
          <th scope="col">Date</th>
          <th scope="col">Invoice</th>
          <th scope="col">Total</th>
      </tr>
  </thead>
  <tbody>
      @foreach($profits as $profit)
      <tr>
          <td>{{ $profit->created_at }}</td>
          <td>{{ $profit->transaction->invoice }}</td>
          <td>{{ formatPrice($profit->total) }}</td>
      </tr>
      @endforeach
      <tr>
          <td colspan="2" class="text-end fw-bold" style="background-color: #D7DBDD">TOTAL</td>
          <td class="text-end fw-bold" style="background-color: #D7DBDD">{{ formatPrice($total) }}</td>
      </tr>
  </tbody>
</table>

<style>
  tbody tr td {
    text-align: center;
  }
</style>