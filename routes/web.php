<?php

use App\Http\Controllers\Apps\CategoryController;
use App\Http\Controllers\Apps\CustomerController;
use App\Http\Controllers\Apps\DashboardController;
use App\Http\Controllers\Apps\PermissionController;
use App\Http\Controllers\Apps\ProductController;
use App\Http\Controllers\Apps\ProfitController;
use App\Http\Controllers\Apps\RoleController;
use App\Http\Controllers\Apps\SalesController;
use App\Http\Controllers\Apps\TransactionController;
use App\Http\Controllers\Apps\UserController;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Auth/Login');
})->middleware(['guest']);

Route::middleware(['auth'])->group(function () {
    # dashboard
    Route::get('/apps/dashboard', DashboardController::class)->name('apps.dashboard');
    # permissions
    Route::get('/apps/permissions', PermissionController::class)->name('apps.permissions.index')
        ->middleware('permission:permissions.index');
    # roles
    Route::resource('/apps/roles', RoleController::class, ['as' => 'apps'])->except(['show'])
        ->middleware('permission:roles.index|roles.create|roles.edit|roles.delete');
    # users
    Route::resource('/apps/users', UserController::class, ['as' => 'apps'])->except(['show'])
        ->middleware('permission:users.index|users.create|users.edit|users.delete');

    # categories
    Route::resource('/apps/categories', CategoryController::class, ['as' => 'apps'])->except(['show']);
    
    # products
    Route::resource('/apps/products', ProductController::class, ['as' => 'apps'])->except(['show']);

    # customers
    Route::resource('/apps/customers', CustomerController::class, ['as' => 'apps'])->except(['show'])
        ->middleware('permission:customers.index|customers.create|customers.edit|customers.delete');

    # transaction
    Route::get('/apps/transactions', [TransactionController::class, 'index'])->name('apps.transactions.index');

    # transaction searchProduct
    Route::post('/apps/transactions/searchProduct', [TransactionController::class, 'searchProduct'])->name('apps.transactions.searchProduct');

    # transaction addToCart
    Route::post('/apps/transactions/addToCart', [TransactionController::class, 'addToCart'])->name('apps.transactions.addToCart');

    # transaction destroyCart
    Route::post('/apps/transactions/destroyCart', [TransactionController::class, 'destroyCart'])->name('apps.transactions.destroyCart');

    # transaction store
    Route::post('/apps/transactions/store', [TransactionController::class, 'store'])->name('apps.transactions.store');

    # transaction print
    Route::get('/apps/transactions/print', [TransactionController::class, 'print'])->name('apps.transactions.print');

    # sales index
    Route::get('/apps/sales', [SalesController::class, 'index'])->middleware('permission:sales.index')->name('apps.sales.index');

    # sales filter
    Route::get('/apps/sales/filter', [SalesController::class, 'filter'])->name('apps.sales.filter');

    # export excel
    Route::get('/apps/sales/export', [SalesController::class, 'export'])->name('apps.sales.export');

    # print pdf
     Route::get('/apps/sales/pdf', [SalesController::class, 'pdf'])->name('apps.sales.pdf');

    # profits
    Route::get('/apps/profits', [ProfitController::class, 'index'])->middleware(['permission:profits.index'])->name('apps.profits.index');

    # profits filter 
    Route::get('/apps/profits/filter', [ProfitController::class, 'filter'])->name('apps.profits.filter');

    # profits export
    Route::get('/apps/profits/export', [ProfitController::class, 'export'])->name('apps.profits.export');

    # profits pdf
    Route::get('/apps/profits/pdf', [ProfitController::class, 'pdf'])->name('apps.profits.pdf');
});
