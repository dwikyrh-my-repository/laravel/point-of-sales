<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    # table
    protected $table = 'products';

    # fillable
    protected $fillable = [
        'category_id', 'image', 'barcode', 'name', 'slug', 'description', 'buy_price',
        'sell_price', 'stock'
    ];

    # relations table
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    # accessor image
    protected function image(): Attribute
    {
        return Attribute::make(
            get: fn($value) => asset('/storage/products/img/'. $value),
        );
    }

    # route key name
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
