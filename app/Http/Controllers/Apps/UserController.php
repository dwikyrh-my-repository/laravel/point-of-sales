<?php

namespace App\Http\Controllers\Apps;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Inertia\Inertia;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        # get users
        $users = User::when(request()->q, function ($users) {
            $users->where('name', 'like',  '%' . request()->q . '%')
                ->orWhere('email', 'like',  '%' . request()->q . '%')
                ->orWhereRelation('roles', 'name', 'like',  '%' . request()->q . '%');
        })->with('roles')->latest()->paginate(20);

        # return with inertia
        return Inertia::render('Apps/Users/Index', [
            'users' => $users,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        # get roles
        $roles = Role::all();

        # return with inertia
        return Inertia::render('Apps/Users/Create', [
            'roles' => $roles
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        # validation
        $request->validate([
            'name' => ['required', 'min:3', 'max:255'],
            'email' => ['required', Rule::unique('users', 'email')],
            'password' => ['required',  'confirmed', 'min:8'],
            'roles' => ['required'],
        ]);

        # create user
        $user = User::create([
            'name'     => $request->name,
            'email'    => $request->email,
            'password' => bcrypt($request->password)
        ]);

        # assign role to user
        $user->assignRole($request->roles);

        # redirect
        return to_route('apps.users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        # get roles
        $roles = Role::all();

        # return with inertia
        return Inertia::render('Apps/Users/Edit', [
            'user' => $user,
            'roles' => $roles,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        # validation
        $request->validate([
            'name' => ['required', 'min:3', 'max:255'],
            'email' => ['required', 'email', Rule::unique('users', 'email')->ignore($user)],
            'password' => ['nullable', 'confirmed'],
        ]);

        # user upadate
        $password = $request->password;
        if ($password == '') {
            $password = $user->password;
        };

        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($password),
        ]);

        # assign roles to user
        $user->syncRoles($request->roles);

        # redirect
        return to_route('apps.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        # delete user
        $user->delete();

        # redirect
        return to_route('apps.users.index');
    }
}
