<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    # table
    protected $table = 'categories';

    # fillable
    protected $fillable = [
        'image', 'name', 'slug', 'description'
    ];

    # relations table
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    # accessor image
    protected function image(): Attribute
    {
        return Attribute::make(
            get: fn($value) => asset('/storage/categories/img/' . $value),
        );
    }

    # route key name
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
