<!DOCTYPE html>
<html lang="en">

<head>
  {{-- default basic html --}}
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  {{-- css admin template --}}
  <link href="{{ asset('assets/admin/css/style.css') }}" rel="stylesheet" />
  {{-- bootstrap 5 css --}}
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
  {{-- css custom --}}
  <link href="{{ asset('assets/admin/css/custom.css') }}" rel="stylesheet" />
  {{-- google fonts --}}
  <link rel="preconnect" href="https://fonts.googleapis.com" />
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
  <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600&display=swap" rel="stylesheet" />
  {{-- font awesome 5 --}}
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" />
  {{-- script admin template --}}
  <script src="{{ asset('assets/admin/js/coreui.bundle.min.js') }}" defer></script>
  {{-- vite --}}
  @vite(['resources/css/app.css', 'resources/js/app.js'])
  {{-- inertia head --}}
  @inertiaHead
</head>

<body>
  @inertia

  {{-- bootstrap 5 js --}}
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
</body>

</html>
