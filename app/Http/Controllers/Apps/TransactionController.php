<?php

namespace App\Http\Controllers\Apps;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Customer;
use App\Models\Product;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Str;

class TransactionController extends Controller
{
    public function index()
    {
        # get carts
        $carts = Cart::with('product')->where('cashier_id', auth()->user()->id)->latest()->get();

        # get all customers
        $customers = Customer::latest()->get();

        # return with inertia
        return Inertia::render('Apps/Transactions/Index', [
            'carts' => $carts,
            'customers' => $customers,
            'carts_total' => $carts->sum('price'),
        ]);
    }

    # search Product
    public function searchProduct(Request $request)
    {
        # find product by barcode
        $product = Product::where('barcode', $request->barcode)->first();

        if ($product) {
            return response()->json([
                'success' => true,
                'product' => $product,
            ]);
        } else {
            return response()->json([
                'success' => false,
                'product' => null,
            ]);
        }
    }

    # add to cart
    public function addToCart(Request $request)
    {
        # check stock product
        if (Product::whereId($request->product_id)->first()->stock < $request->quantity) {
            # redirect
            return back()->with('error', 'Out of Stock Product');
        }

        # check chart
        $cart = Cart::with('product')
            ->where('product_id', $request->product_id)
            ->where('cashier_id', auth()->user()->id)
            ->first();

        if ($cart) {
            # increment quantity
            $cart->increment('quantity', $request->quantity);

            # sum price * quantity
            $cart->price = $cart->product->sell_price * $cart->quantity;

            # save to cart
            $cart->save();
        } else {
            # insert cart
            Cart::create([
                'cashier_id' => auth()->user()->id,
                'product_id' => $request->product_id,
                'quantity' => $request->quantity,
                'price' => $request->sell_price * $request->quantity,
            ]);
        }

        # redirect
        return to_route('apps.transactions.index')->with('success', 'Product has been added.');
    }

    # destroy cart
    public function destroyCart(Request $request)
    {
        # find cart by id
        $cart = Cart::with('product')
        ->whereId($request->cart_id)
        ->first();

        # delete cart
        $cart->delete();

        # redirect
        return back()->with('success', 'Product has been removed.');
    }

    # store
    public function store(Request $request)
    {
        # algorithm generate no invoice
        $length = 10;
        $random = '';
        for($i = 0; $i < $length; $i++) {
            $random .= rand(0,1) ? rand(0,9) : chr(rand(ord('a'), ord('z')));
        }

        # generate no invoice
        $invoice = 'TRX-' . Str::upper($random);

        # insert transaction
        $transaction = Transaction::create([
            'cashier_id' => auth()->user()->id,
            'customer_id' => $request->customer_id,
            'invoice' => $invoice,
            'cash' => $request->cash,
            'change' => $request->change,
            'discount' => $request->discount,
            'grand_total' => $request->grand_total,
        ]);

        # get carts
        $carts = Cart::where('cashier_id', auth()->user()->id)->get();

        # insert transaction detail
        foreach($carts as $cart) {
            # insert transaction detail
            $transaction->details()->create([
                'transaction_id' => $transaction->id,
                'product_id' => $cart->product_id,
                'quantity' => $cart->quantity,
                'price' => $cart->price,
            ]);

            # get price
            $total_buy_price = $cart->product->buy_price * $cart->quantity;
            $total_sell_price = $cart->product->sell_price * $cart->quantity;

            # get profits
            $profits = $total_sell_price - $total_buy_price;

            # insert provits
            $transaction->profits()->create([
                'transaction_id' => $transaction->id,
                'total' => $profits,
            ]);

            # update stock product
            $product = Product::find($cart->product_id);
            $product->stock = $product->stock - $cart->quantity;
            $product->save();
        }

        # delete carts
        Cart::where('cashier_id', auth()->user()->id)->delete();
        return response()->json([
            'success' => true,
            'data' => $transaction,
        ]);
    }

    # print
    public function print(Request $request)
    {
        # get transaction
        $transaction = Transaction::with('details.product', 'cashier', 'customer')->where('invoice', $request->invoice)->firstOrFail();

        # return view
        return view('print.nota', compact('transaction'));
    }
}
