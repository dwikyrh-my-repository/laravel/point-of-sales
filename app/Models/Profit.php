<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profit extends Model
{
    use HasFactory;

    # table
    protected $table = 'profits';

    # fillable
    protected $fillable = [
        'transaction_id', 'total'
    ];

    # relations table
    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }

     # accessor date
     protected function createdAt(): Attribute
     {
         return Attribute::make(
             get: fn($value) => Carbon::parse($value)->format('d-M-Y H:i:s'),
         );
     }
}
